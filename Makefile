diapo:
	mkdir -p build dist
	pdflatex -output-directory build presentazione.tex
	mv build/presentazione.pdf dist

clean:
	rm -rf build
